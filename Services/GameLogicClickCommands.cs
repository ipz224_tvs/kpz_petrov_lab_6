﻿using Checkers.Enums;
using Checkers.Models;
using System.Collections.ObjectModel;


namespace Checkers.Services;

public partial class GameLogicClickCommands : GameLogicBase
{
    public GameLogicClickCommands(ObservableCollection<ObservableCollection<GameSquare>> board, PlayerTurn playerTurn, Winner winner) : base(board, playerTurn, winner)
    {
    }

    public void ResetGame()
    {
        Utility.ResetGame(board);
    }

    public void SaveGame()
    {
        Utility.SaveGame(board);
    }

    public void LoadGame()
    {
        Utility.LoadGame(board);
        playerTurn.TurnImage = Utility.Turn.TurnImage;
    }

    public void ClickPiece(GameSquare square)
    {
        if ((Utility.Turn.PlayerColor == PieceColor.Red && square.Piece.Color == PieceColor.Red ||
            Utility.Turn.PlayerColor == PieceColor.White && square.Piece.Color == PieceColor.White) &&
            !Utility.ExtraMove)
        {
            DisplayRegularMoves(square);
        }
    }
}
